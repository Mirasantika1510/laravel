<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/register/create', 'Auth\RegisterController@register')->name('register');
Route::post('/login', 'Auth\LoginController@login');

Route::middleware('auth')->group(function() {
	Route::get('/users', 'HomeController@index')->name('users');
	Route::post('/shopping', 'ShoppingController@store')->name('shopping');
	Route::get('/shopping', 'ShoppingController@index')->name('shopping');
	Route::get('/shopping/{shopping}', 'ShoppingController@show')->name('shopping');
	Route::post('/shopping/{shopping}/update', 'ShoppingController@update')->name('shopping');
	Route::get('/shopping/{shopping}/delete', 'ShoppingController@destroy')->name('shopping');
});
