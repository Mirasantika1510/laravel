<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('auth')->group(function() {
	Route::get('/users', 'HomeController@index')->name('users');
	Route::post('/shopping', 'ShoppingController@store')->name('shopping');
	Route::get('/shopping', 'ShoppingController@index')->name('shopping');
	Route::get('/shopping/{shopping}', 'ShoppingController@show')->name('shopping');
	Route::post('/shopping/{shopping}/update', 'ShoppingController@update')->name('shopping');
	Route::get('/shopping/{shopping}/delete', 'ShoppingController@destroy')->name('shopping');
});