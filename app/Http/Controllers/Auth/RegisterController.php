<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
	public function showRegistrationForm()
	{
		return view('auth.register');
	}

	public function register(Request $request)
	{
		$request->validate([
			'username'	=> 'required|unique:users',
			'email'		=> 'required|unique:users|email|max:255',
			'password'	=> 'required|string|min:8|confirmed',
			'phone'		=> 'required|numeric',
			'address'	=> 'required',
			'city'		=> 'required',
			'country'	=> 'required',
			'name'		=> 'required',
			'postcode'	=> 'required|max:5',
		]);

		$data = User::create([
			'username'	=> $request->username,
			'email'		=> $request->email,
			'password'	=> bcrypt($request->password),
			'phone'		=> $request->phone,
			'address'	=> $request->address,
			'city'		=> $request->city,
			'country'	=> $request->country,
			'name'		=> $request->name,
			'postcode'	=> $request->postcode,
			'token'		=> Str::random(60),
		]);

		return response()->json(['user' => $data]);
	}
}
