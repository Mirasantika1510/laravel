<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shopping;

class ShoppingController extends Controller
{
	public function index()
	{
		$shopping = Shopping::all();

		return response()->json(['data' => $shopping]);
	}

	public function store(Request $request)
	{
		$request->validate([
			'name'      => 'required',
		]);

		$data = [
			'name'			=> $request->name,
			'created_at'	=> $request->createddate
		];

		$insert = Shopping::create($data);

		return response()->json(['data' => $insert]);
	}

	public function show(Shopping $shopping)
	{
		return response()->json(['data' => $shopping]);
	}

	public function update(Shopping $shopping, Request $request)
	{
		$request->validate([
			'name'      => 'required',
		]);

		$update = $shopping->update([
			'name'	=> $request->name
		]);

		return response()->json(['shopping' => $update]);

	}

	public function destroy(Shopping $shopping)
	{
		$shopping->delete();
	}
}
